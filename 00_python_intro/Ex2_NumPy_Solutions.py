#!/usr/bin/env python
# coding: utf-8

# # Exercise 2 - Learning NumPy - Solutions
# `numpy` is the basis for numerical scientific computing in Python. If you want to work with data in Python, understanding how NumPy works is key.
# 
# It contains among other things:
# 
# - a powerful N-dimensional array object
# - sophisticated (broadcasting) functions
# - tools for integrating C/C++ and Fortran code
# - useful linear algebra, Fourier transform, and random number capabilities
# 
# You can find the numpy reference [here](http://docs.scipy.org/doc/numpy/reference/index.html#reference).

# 1) Import the numpy package under the name ``np``

# In[1]:


import numpy as np


# 2) Create a null vector of size 10 (Hint: See
# [`np.zeros`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.zeros.html))

# In[2]:


a = np.zeros(10)
a


# 3) Create a null vector of size 10 except the fifth value, which is supposed to be 1

# In[3]:


a[4] = 1.
a


# 4) Create a vector with values ranging from 10 to 99 (Hint: See [`np.arange`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.arange.html))

# In[4]:


b = np.arange(10,100)
b


# 5) Create a 3x3 matrix with values ranging from 0 to 8 (Hint: See [`np.reshape`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.reshape.html#numpy.ndarray.reshape))

# In[5]:


A = np.arange(9)
A = A.reshape(3,3)
A


# 6) Find indices of non-zero elements from \[1,2,0,0,4,0\] (Hint: See
# [`np.nonzero`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.nonzero.html))

# In[6]:


ind = np.nonzero([1,2,0,0,4,0])
ind


# 7) Declare a 10x10 identity matrix (Hint: See [`np.eye`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.eye.html))

# In[7]:


I = np.eye(10)
I


# 8) Declare a 10x10x10 array with random values (Hint: See [Random
# sampling](https://docs.scipy.org/doc/numpy/reference/random/index.html))

# In[8]:


Rand10 = np.random.random((10,10,10))
print("Shape of the array:", Rand10.shape)


# In[9]:


# Alternatively, if you prefer random integer values:
Rand10 = np.random.randint(100, size=(10,10,10))
print("Shape of the array:", Rand10.shape)


# 9) Create a 10x10 matrix with row values ranging from 0 to 9 (Hint: Make use of [Broadcasting](https://docs.scipy.org/doc/numpy/user/basics.broadcasting.html))

# In[10]:


B = np.ones((10,10))
scale = np.arange(10)
B *= scale
B


# 10) Create a random vector of size 100 and sort it (Hint: See [Sorting, searching and counting](https://docs.scipy.org/doc/numpy/reference/routines.sort.html))

# In[11]:


rand100 = np.random.random(100)
rand100 = np.sort(rand100)
rand100


# 11) Create a random vector with 1000 entries and calculate its mean, median, and standard deviation. (Hint: See [Statistics](https://docs.scipy.org/doc/numpy/reference/routines.statistics.html))

# In[12]:


rand1000 = np.random.random(1000)
print("Mean:", np.mean(rand1000))
print("Median:", np.median(rand1000))
print("Standard deviation:", np.std(rand1000))


# 12) Create a random vector of size 100 and replace the maximum value by 05 (Hint: See [Sorting, searching and counting](https://docs.scipy.org/doc/numpy/reference/routines.sort.html))

# In[13]:


rand100 = np.random.random(100)
rand100[np.argmax(rand100)] = 5.
rand100


# 13) Reverse a vector (Hint: `[::-1]`)

# In[14]:


x = np.arange(10,100)
x_rev = b[::-1]
x_rev


# 14) Create a 1-D array of 50 evenly spaced elements between 3. and 10., inclusive. (Hint: Use [`np.linspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html))

# In[15]:


x_lin = np.linspace(3, 10, 50)
x_lin


# 15) Create a 1-D array of 50 element spaced evenly on a log scale between 3. and 10., exclusive. (Hint: Use [`np.logspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.logspace.html) or [`np.geomspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.geomspace.html))

# In[16]:


x_log = np.geomspace(3, 10, num=50, endpoint=False)
x_log


# 16) Create two two-dimensional arrays. Multiply them element-wise (`*`) and calculate the dot product (`@`).

# In[17]:


A = np.arange(9)
A = A.reshape(3,3)
B = A.copy()
print("Element-wise multiplication:")
print( A * B)
print("Dot product:")
print(A @ B)


# ___________________________________________________________________________________________________________________
# 
# ## Bonus Questions

# 17) Calculate sine, cosine, and tangent of x, element-wise. (Hint: See [Mathematical functions](https://docs.scipy.org/doc/numpy/reference/routines.math.html))

# In[18]:


x = np.array([0., 1., 30, 90])

print("Sine of x:", np.sin(x))
print("Cosine of x:", np.cos(x))
print("Tangent of x:", np.tan(x))


# 18) Create a random array (20 x 20) and declare a value. Now find the closest value (to the given scalar) in that array. (Hint: Use [`np.abs`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.absolute.html) and [`np.argmin`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.argmin.html))

# In[19]:


Rand20 = np.random.random((20,20))
value = 0.5
dist = np.abs(Rand20 - value)
ind = np.where(dist == dist.min())
print(Rand20[ind])


# 19) Convert x from radian to degrees. Search for the corresponding numpy function with [`np.lookfor`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.lookfor.html).

# In[20]:


x = np.array([-np.pi, -np.pi/2, np.pi/2, np.pi])


# In[21]:


np.lookfor("radians degrees")


# In[22]:


x = np.rad2deg(x)
x

